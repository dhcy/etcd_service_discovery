module gitee.com/dhcy/etcd_service_discovery

go 1.16

require (
	go.etcd.io/etcd/api/v3 v3.5.0
	go.etcd.io/etcd/client/v3 v3.5.0
)
