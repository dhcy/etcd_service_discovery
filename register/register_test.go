package register

import (
	"testing"
	"time"
)

func TestNewServiceRegister(t *testing.T) {
	ser, err := NewServiceRegister([]string{"127.0.0.1:2379"}, "/web/nginx_ip", "https://172.168.10.8:443", 2)
	if err != nil {
		t.Fatal(err)
	}

	go func() {
		leaseRespChan := ser.ListenLeaseRespChan()
		for response := range leaseRespChan {
			t.Logf("lease ok id:%d", response.ID)
		}
	}()

	select {
	case <-time.After(30 * time.Second):
		_ = ser.Close()
	}
}
