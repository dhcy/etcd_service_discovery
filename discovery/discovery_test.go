package discovery

import (
	"testing"
	"time"
)

// TestNewServiceDiscovery 测试服务发现
func TestNewServiceDiscovery(t *testing.T) {
	ser := NewServiceDiscovery([]string{"127.0.0.1:2379"})

	_ = ser.WatchService("/web")
	ch := make(chan int,1)
	go func() {
		for {
			select {
			case <-ch:
				services := ser.GetServices()
				t.Logf("find service:%v", services)
			}
		}
	}()
	for i := 0; i < 30; i++ {
		ch <- i
		time.Sleep(1 * time.Second)
	}

}
